package com.algorithms.test.lists;

import com.algorithms.lists.Node;
import com.algorithms.lists.RemoveNonUnique;
import com.algorithms.test.utils.AssertListUtils;
import com.algorithms.test.utils.LinkedListUtils;
import org.junit.Test;

/**
 * Created by andrejp on 08.07.17.
 */
public class RemoveNonUniqueTest {

    @Test
    public void testRegularList() throws Exception {
        Node<Integer> head = LinkedListUtils.linkedListAssembly(1, 2, 3, 1, 4);

        RemoveNonUnique r = new RemoveNonUnique();

        Node<Integer> res = r.unique(head);

        AssertListUtils.assertLinkedList(res, 1,2,3,4);
    }


    @Test
    public void testAllNodesAreEqual() throws Exception {
        Node<Integer> head = LinkedListUtils.linkedListAssembly(1, 1, 1, 1, 1);

        RemoveNonUnique r = new RemoveNonUnique();

        Node<Integer> res = r.unique(head);

        AssertListUtils.assertLinkedList(res, 1);
    }

    @Test
    public void testTwoNeighboringNodesAreEqual() throws Exception {
        Node<Integer> head = LinkedListUtils.linkedListAssembly(1, 2, 2, 3, 4);

        RemoveNonUnique r = new RemoveNonUnique();

        Node<Integer> res = r.unique(head);

        AssertListUtils.assertLinkedList(res, 1,2, 3,4);
    }

    @Test
    public void testFirstAndLastNodesAreEquals() throws Exception {
        Node<Integer> head = LinkedListUtils.linkedListAssembly(1, 2, 3, 4, 1);

        RemoveNonUnique r = new RemoveNonUnique();

        Node<Integer> res = r.unique(head);

        AssertListUtils.assertLinkedList(res, 1,2, 3,4);
    }

    @Test
    public void testPairOfEqualNodes() throws Exception {
        Node<Integer> head = LinkedListUtils.linkedListAssembly(1, 2, 3, 2, 1);

        RemoveNonUnique r = new RemoveNonUnique();

        Node<Integer> res = r.unique(head);

        AssertListUtils.assertLinkedList(res, 1, 2, 3);
    }
}
