package com.algorithms.test.lists;

import com.algorithms.lists.Node;
import com.algorithms.lists.RevertLinkedList;
import com.algorithms.test.utils.AssertListUtils;
import com.algorithms.test.utils.LinkedListUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by andrejp on 04.07.17.
 */


public class RevertLinkedListTest {

    @Test
    public void testRevertSingleNodeList() throws Exception {

        Node<Integer> n1 = new Node<>(1);

        Assert.assertEquals(n1, RevertLinkedList.revert(n1));
    }

    @Test
    public void testRevertStandardList() throws Exception {

        Node<Integer> n1 = LinkedListUtils.linkedListAssembly(1, 2, 3, 4);

        Node<Integer> res = RevertLinkedList.revert(n1);

        AssertListUtils.assertLinkedList(res, 4, 3, 2, 1);

    }
}
