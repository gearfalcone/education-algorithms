package com.algorithms.test.lists;

import com.algorithms.lists.Node;
import com.algorithms.lists.SumLinkedLists;
import com.algorithms.test.utils.AssertListUtils;
import com.algorithms.test.utils.LinkedListUtils;
import org.junit.Test;

/**
 * Created by Andrey Pendyurin on 12.07.2017.
 */
public class SumLinkedListsTest {

    @Test
    public void testListWithEqualLength() throws Exception{
        Node<Integer> list = LinkedListUtils.linkedListAssembly(9, 9, 9);

        SumLinkedLists inst = new SumLinkedLists();
        Node<Integer> ret = inst.sumLists(list, list);

        AssertListUtils.assertLinkedList(ret, 1, 9, 9, 8);
    }

    @Test
    public void test4NumbersAnd1NumberLists() throws Exception {
        Node<Integer> left = LinkedListUtils.linkedListAssembly(9, 9, 9, 9);
        Node<Integer> right = LinkedListUtils.linkedListAssembly(9);

        SumLinkedLists inst = new SumLinkedLists();
        Node<Integer> ret = inst.sumLists(left, right);

        AssertListUtils.assertLinkedList(ret, 1, 0, 0, 0, 8);
    }

    @Test
    public void test2NumbersAnd4NumberLists() throws Exception {
        Node<Integer> left = LinkedListUtils.linkedListAssembly(9, 9);
        Node<Integer> right = LinkedListUtils.linkedListAssembly(9, 9, 9, 9);

        SumLinkedLists inst = new SumLinkedLists();
        Node<Integer> ret = inst.sumLists(left, right);

        AssertListUtils.assertLinkedList(ret, 1, 0, 0, 9, 8);
    }
}
