package com.algorithms.test.stacks;

import com.algorithms.stacks.StackMin;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by andrejp on 12.07.17.
 */
public class StackMinTest {

    @Test
    public void test1234Remove2Last(){
        StackMin<Integer> st = new StackMin<>();

        st.push(1);
        st.push(2);
        st.push(3);
        st.push(4);

        Assert.assertEquals(1, st.min().intValue());
    }

    @Test
    public void test4EqualsValues(){
        StackMin<Integer> st = new StackMin<>();

        st.push(1);
        st.push(1);
        st.push(1);
        st.push(1);

        Assert.assertEquals(1, st.min().intValue());
    }

    @Test
    public void testSecondMinShouldBe2(){
        StackMin<Integer> st = new StackMin<>();

        st.push(1);
        st.push(2);
        st.push(3);
        st.push(4);

        Assert.assertEquals(4, st.pop().intValue());

        Assert.assertEquals(1, st.min().intValue());
    }

    @Test
    public void testMinAfterSecondInsert(){
        StackMin<Integer> st = new StackMin<>();

        st.push(4);
        st.push(3);
        st.push(2);
        st.push(1);

        st.pop();
        st.pop();

        st.push(0);
        st.push(5);

        Assert.assertEquals(0, st.min().intValue());
    }

    @Test
    public void testInsertMinThatCurrentMin(){
        StackMin<Integer> st = new StackMin<>();

        st.push(5);
        st.push(4);
        st.push(3);
        st.push(2);

        st.push(1);

        Assert.assertEquals(1, st.min().intValue());
    }

}
