package com.algorithms.test.stacks;

import com.algorithms.stacks.SortedStack;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by andrejp on 15.07.17.
 */
public class SortedStackTest {

    @Test
    public void testSingleElement(){
        SortedStack<Integer> stack = new SortedStack<>();
        stack.push(10);

        Assert.assertEquals(10, stack.pop().intValue());
    }

    @Test
    public void testTenEqualsElements(){
        SortedStack<Integer> stack = new SortedStack<>();

        for(int i=0;i<10;i++) stack.push(10);

        for(int i=0;i<10;i++) Assert.assertEquals(10, stack.pop().intValue());
    }

    @Test
    public void testInputReverseOrder(){
        SortedStack<Integer> stack = new SortedStack<>();
        for(int i=1;i<10;i++) stack.push(i);

        for(int i=1;i<10;i++) Assert.assertEquals(i, stack.pop().intValue());
    }

    @Test
    public void testOrderingAfterPop(){
        SortedStack<Integer> stack = new SortedStack<>();
        for(int i=1;i<=5;i++) stack.push(i);

        Assert.assertEquals(1, stack.pop().intValue());

        stack.push(6);
        stack.push(7);

        Assert.assertEquals(2, stack.pop().intValue());
        Assert.assertEquals(3, stack.pop().intValue());
        Assert.assertEquals(4, stack.pop().intValue());
        Assert.assertEquals(5, stack.pop().intValue());
        Assert.assertEquals(6, stack.pop().intValue());
        Assert.assertEquals(7, stack.pop().intValue());
    }
}
