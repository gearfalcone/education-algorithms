package com.algorithms.test.utils;

import com.algorithms.lists.Node;

import java.util.Arrays;

/**
 * Created by andrejp on 08.07.17.
 */
public class LinkedListUtils {

    public static <T> String toString(Node<T> head) {
        Node<T> k = head;
        StringBuilder ret = new StringBuilder();

        for (; k.next != null; k = k.next) {
            ret.append(k.toString() + "->");
        }

        ret.append(k);

        return ret.toString();
    }

    public static <T> Node<T> linkedListAssembly(T... nodes) {
        Node<T> prev = null;
        Node<T> head = null;
        for(int i=0;i<nodes.length;i++){
            Node<T> n = new Node<T>(nodes[i]);
            if(head == null) head = n;
            if(prev == null) prev = n;
            else prev.next = n;
            prev = n;
        }

        return head;
    }

}
