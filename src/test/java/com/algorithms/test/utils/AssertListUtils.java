package com.algorithms.test.utils;

import com.algorithms.lists.Node;
import junit.framework.AssertionFailedError;

/**
 * Created by andrejp on 08.07.17.
 */
public class AssertListUtils {

    public static <T> void assertLinkedList(Node<T> actual, T... expected) {
        if (actual == null && expected.length != 0)
            throw new AssertionFailedError("actual is null");

        assertLinkedListSize(actual, expected.length);

        Node<T> a = actual;

        for (int i = 0; a.next != null; i++, a = a.next) {
            if (!a.value.equals(expected[i]))
                throw new AssertionFailedError(String.format("value %s doesn't equal to %s. Actual list: %s",
                        a.value.toString(),
                        expected[i].toString(),
                        LinkedListUtils.toString(actual)));
        }
    }

    public static <T> void assertLinkedListSize(Node<T> head, int expected) {
        if (head == null && expected != 0)
            throw new AssertionFailedError(
                    String.format("Wrong size of the list. Actual %d, expected %d. Actual list: %s", 0, expected, LinkedListUtils.toString(head)));

        int i = 0;
        Node<T> t = head;
        while (t !=null){
            i++;
            t = t.next;
        }
        if (i != expected)
            throw new AssertionFailedError(
                    String.format("Wrong size of the list. Actual %d, expected %d. Actual list: %s", i, expected, LinkedListUtils.toString(head)));
    }
}
