package com.algorithms.lists;

/**
 * Created by Andrey Pendyurin on 12.07.2017.
 */
public class SumLinkedLists {

    private NodeWrapper sum(Node<Integer> left, Node<Integer> right) {
        int saved = 0;
        NodeWrapper next;

        if(left == null && right == null) return null;
        else next = sum(left.next, right.next);

        if(next != null) saved = next.saved;

        int s = left.value + right.value + saved;

        if(s>=10){
            s%=10;
            saved = 1;
        } else saved = 0;

        return new NodeWrapper(s, saved, next);
    }

    private int length(Node<Integer> head){
        int ret = 0;
        Node<Integer> n = head;
        for(;n!=null;n=n.next, ret++);
        return ret;
    }

    private Node<Integer> addLeadZeros(Node<Integer> head, int actLength, int expLength) {
        Node<Integer> ret = head;
        for(int i = 0; i< expLength - actLength; i++){
            ret = new Node<>(0, ret);
        }

        return ret;
    }

    public Node sumLists(Node<Integer> leftHead, Node<Integer> rightHead) {
        int leftLength = length(leftHead);
        int rightLength = length(rightHead);

        Node<Integer> left, right;
        if(leftLength > rightLength) {
            left = leftHead;
            right = addLeadZeros(rightHead, rightLength, leftLength);
        } else if(rightLength > leftLength) {
            right = rightHead;
            left = addLeadZeros(leftHead, leftLength, rightLength);
        } else {
            left = leftHead;
            right = rightHead;
        }

        NodeWrapper r = sum(left, right);
        if(r.saved == 1){
            NodeWrapper n = new NodeWrapper(1,0,r);
            r = n;
        }

        return toNode(r);
    }

    private Node toNode(NodeWrapper head){
        NodeWrapper n = head;
        Node<Integer> current, prev=null;
        Node<Integer> newHead = null;
        while(n!=null){
            current = new Node<>(n.value);
            if(prev!=null) prev.next = current;
            prev = current;
            if(n == head) newHead = current;
            n = n.next;
        }

        return newHead;
    }

    private static class NodeWrapper{
        int value, saved;
        NodeWrapper next;

        NodeWrapper(int value, int saved, NodeWrapper next){
            this.value = value;
            this.saved = saved;
            this.next = next;
        }
    }
}
