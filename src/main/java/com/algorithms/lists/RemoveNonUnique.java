package com.algorithms.lists;

/**
 * Created by andrejp on 04.07.17.
 */
public class RemoveNonUnique {

    @SuppressWarnings("PMD")
    public Node unique(final Node head) {
        Node p1 = head;
        Node p2, prev;

        while (p1 != null) {
            prev = p1;
            p2 = p1.next;
            while (p2 != null) {
                if (p1.value == p2.value) {
                    p2 = p2.next;
                    prev.next = prev.next.next;
                } else {
                    p2 = p2.next;
                    prev = prev.next;
                }
            }
            p1 = p1.next;
        }

        return head;
    }

}
