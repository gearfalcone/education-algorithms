package com.algorithms.lists;

/**
 * Created by andrejp on 04.07.17.
 */
public class Node<T> {

    public T value;
    public Node<T> next;


    public Node(T value, Node<T> next) {
        this.value = value;
        this.next = next;
    }

    public Node(T value) {
        this.value = value;
        this.next = null;
    }

    @Override
    public String toString(){
        return value.toString();
    }
}
