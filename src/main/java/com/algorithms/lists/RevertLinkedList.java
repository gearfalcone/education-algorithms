package com.algorithms.lists;

public class RevertLinkedList {

    public static <T> Node<T> revert(Node<T> head) {
        Node cur = head;
        Node next, prev=null;

        while(cur != null) {
            next = cur.next;
            cur.next = prev;
            prev=cur;
            cur = next;
        }

        return prev;
    }

}