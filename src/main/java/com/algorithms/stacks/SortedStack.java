package com.algorithms.stacks;

import java.util.Stack;

/**
 * Created by andrejp on 15.07.17.
 */
public class SortedStack <T extends Comparable> extends Stack<T> {

    private Stack<T> tmp = new Stack<>();

    @Override
    public T push(T el){

        while(!isEmpty() && peek().compareTo(el) <= 0){
            tmp.push(pop());
        }

        super.push(el);

        while(!tmp.isEmpty()){
            super.push(tmp.pop());
        }

        return el;
    }
}
