package com.algorithms.stacks;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * Created by andrejp on 12.07.17.
 */
public class StackMin <T extends Comparable> {

    private Stack<T> main = new Stack<>();
    private Stack<T> mins = new Stack<>();

    public void push(T el){
        if(mins.empty()) mins.push(el);
        else if(mins.peek().compareTo(el)>=0){
            mins.push(el);
        }

        main.push(el);
    }

    public T pop(){
        T el = main.pop();

        if(mins.peek().compareTo(el) == 0) mins.pop();

        return el;
    }

    public T min(){
        return mins.peek();
    }
}
